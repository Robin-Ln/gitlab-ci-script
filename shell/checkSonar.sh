#!/bin/bash

cd ..
venv/bin/python3 -m src.main.check_sonar "$1"

res=$?
if [[ $res == 0 ]]; then
    echo "Succeed"
else
    echo "Failed"
fi