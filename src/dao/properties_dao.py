from src.constant.contant_utils import PROPERTIES
from src.model.properties_model import SonarqubeProperties


class PropertiesDao:
    """
    Dao qui permet la lecture de fichier de properties.
    """

    def __init__(self):
        """
        Constructeur sans paramètre.
        """
        self.separator = PROPERTIES.get("separator")

    def read_properties(self, path_file):
        """
        Lecture d'un fichier de properties.
        :param path_file : Chemin d'accès du fichier de properties.
        :type path_file : str
        :return Retourne un dictionnaire avec en clées/valeures le contenue du fichier de properties.
        :rtype dict
        """
        properties = {}
        with open(path_file) as file:
            for line in file:
                if self.separator in line:
                    name, value = line.split(self.separator, 1)
                    properties[name.strip()] = value.strip()
        return properties

    def read_sonarqube_properties(self, path_file):
        """
        Lecture du rapport de fait par sonarqube.
        :param path_file : Chemin d'accès au rapport de fait par sonarqube.
        :type path_file : str
        :return Retourne un objet de type SonarqubeProperties.
        :rtype SonarqubeProperties
        """
        properties = self.read_properties(path_file)
        return SonarqubeProperties(properties)


if __name__ == "__main__":
    param_path_file = "/Users/robinlouarn/git/http-logger/build/sonar/report-task.txt"
    dao = PropertiesDao()
    print(dao.read_properties(param_path_file))
    print(dao.read_sonarqube_properties(param_path_file))
