import requests
from src.constant.env import ENV
from src.dao.properties_dao import PropertiesDao
from src.model.sonarqube_model import SonarqubeTask


class SonarqubeDao:
    """
    Dao pour les appelles à l'api de sonarqube.
    """

    def __init__(self):
        """
        Constructeur sans paramètres.
        """
        self.env = ENV.get("sonarqube")

    def get_task(self, url):
        """
        Récupération d'un rapport en fonction de son id.
        :param url: url de sonar pour récupérer le rapport.
        :type url: str
        :return Un objet de type SonarqubeTask
        :rtype SonarqubeTask
        """

        headers = self.env.get("headers")

        response = requests.request("GET", url, headers=headers)
        if response.ok:
            return SonarqubeTask(response.json())
        else:
            raise Exception("SonarqubeDao::get_task({})".format(id))


if __name__ == "__main__":
    param_path_file = "/Users/robinlouarn/git/http-logger/build/sonar/report-task.txt"
    dao = PropertiesDao()
    sonarqube_properties = dao.read_sonarqube_properties(param_path_file)
    sonarqubeDao = SonarqubeDao()
    print(sonarqubeDao.get_task(sonarqube_properties.ce_task_url))
