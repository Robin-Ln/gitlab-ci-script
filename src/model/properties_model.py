class SonarqubeProperties:

    def __init__(self, properties):
        self.ce_task_url = properties.get("ceTaskUrl")

    def __str__(self):
        return "ce_task_url: {}".format(self.ce_task_url)


if __name__ == "__main__":
    properties_param = {
        'organization': 'robinlouarn',
        'projectKey': 'fr.louarn:http-logger',
        'serverUrl': 'https://sonarcloud.io',
        'serverVersion': '8.0.0.9382',
        'dashboardUrl': 'https://sonarcloud.io/dashboard?id=fr.louarn%3Ahttp-logger',
        'ceTaskId': 'AXMvggshFqF0v-dl6kLu',
        'ceTaskUrl': 'https://sonarcloud.io/api/ce/task?id=AXMvggshFqF0v-dl6kLu'
    }

    sonarqubeProperties = SonarqubeProperties(properties_param)
    print(sonarqubeProperties)
