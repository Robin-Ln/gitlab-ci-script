class SonarqubeTask:

    def __init__(self, json):
        task = json.get("task")
        self.status = task.get("status")

    def __str__(self):
        return "status {}".format(self.status)


if __name__ == "__main__":
    json_value = {
        "task": {
            "id": "AXMvggshFqF0v-dl6kLu",
            "type": "REPORT",
            "componentId": "AXMumLu_z2_rC7ltPm8H",
            "componentKey": "fr.louarn:http-logger",
            "componentName": "http-logger",
            "componentQualifier": "TRK",
            "analysisId": "AXMvggw8kAL3wbHXxQr8",
            "status": "SUCCESS",
            "submittedAt": "2020-07-08T19:38:38+0200",
            "submitterLogin": "Robin-Ln@gitlab",
            "startedAt": "2020-07-08T19:38:38+0200",
            "executedAt": "2020-07-08T19:38:40+0200",
            "executionTimeMs": 1316,
            "logs": False,
            "hasScannerContext": True,
            "organization": "robinlouarn",
            "warningCount": 0,
            "warnings": []
        }
    }

    sonarqubeTask = SonarqubeTask(json_value)
    print(sonarqubeTask)
