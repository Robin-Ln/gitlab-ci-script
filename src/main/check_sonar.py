from src.dao.properties_dao import PropertiesDao
from src.dao.sonarqube_dao import SonarqubeDao
import sys

if __name__ == "__main__":
    # Récupération du path
    path_file = sys.argv[1]

    # Lecture du fichier de properties
    dao = PropertiesDao()
    sonarqube_properties = dao.read_sonarqube_properties(path_file)

    # Récupération du rapport
    sonarqube_dao = SonarqubeDao()
    task = sonarqube_dao.get_task(sonarqube_properties.ce_task_url)

    if not task.status:
        exit(1)
